onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /fifo_lfsr_tb/r_clock
add wave -noupdate /fifo_lfsr_tb/r_reset
add wave -noupdate /fifo_lfsr_tb/r_wr_data
add wave -noupdate /fifo_lfsr_tb/wr_data
add wave -noupdate /fifo_lfsr_tb/w_full
add wave -noupdate /fifo_lfsr_tb/w_empty
add wave -noupdate /fifo_lfsr_tb/w_afull
add wave -noupdate /fifo_lfsr_tb/r_wr_en
add wave -noupdate /fifo_lfsr_tb/r_rd_en
add wave -noupdate /fifo_lfsr_tb/fifo_del_inst/op_err_o
add wave -noupdate /fifo_lfsr_tb/cnt
add wave -noupdate /fifo_lfsr_tb/fifo_del_inst/fifo_data
add wave -noupdate /fifo_lfsr_tb/w_rd_data
add wave -noupdate /fifo_lfsr_tb/d_idx
add wave -noupdate /fifo_lfsr_tb/d_en_i
add wave -noupdate /fifo_lfsr_tb/d_d_o
add wave -noupdate /fifo_lfsr_tb/set_seed
add wave -noupdate /fifo_lfsr_tb/c_depth
add wave -noupdate /fifo_lfsr_tb/c_width
add wave -noupdate /fifo_lfsr_tb/c_threshold
add wave -noupdate /fifo_lfsr_tb/c_depth_width
add wave -noupdate /fifo_lfsr_tb/uut/range_i
add wave -noupdate /fifo_lfsr_tb/uut/seed_i
add wave -noupdate /fifo_lfsr_tb/uut/rand_o
add wave -noupdate /fifo_lfsr_tb/uut/rnd_gen/rand_temp
add wave -noupdate /fifo_lfsr_tb/uut/rnd_gen/temp
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/wr_en_i
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/rd_en_i
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/del_en_i
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/op_err_o
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/full
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/empty
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/afull
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/ctrl/wr_idx
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/cnt_o
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/del_idx_i
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/del_data_o
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/rd_data_o
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/empty_o
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/fifo_data
add wave -noupdate -expand -group FIFO /fifo_lfsr_tb/fifo_del_inst/wr_data_i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {896000 ps} 0}
configure wave -namecolwidth 224
configure wave -valuecolwidth 90
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {754900 ps} {1389100 ps}
