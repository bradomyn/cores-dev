library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity fifo_linked_list is
  generic (
    g_width       : integer := 8;
    g_depth       : integer := 32;
    g_threshold   : integer := 6;
    g_depth_width : integer := 3
    );
  port (
    clk_i     : in std_logic;
    rst_n_i   : in std_logic;
 
    wr_en_i   : in  std_logic;
    wr_data_i : in  std_logic_vector(g_width-1 downto 0);
    full_o    : out std_logic;
    afull_o   : out std_logic;
    
    del_en_i  : in  std_logic;
    del_idx_i : in  std_logic_vector(g_depth_width-1 downto 0);
    del_data_o: out std_logic_vector(g_width-1 downto 0);

    op_err_o  : out std_logic;
    cnt_o     : out std_logic_vector(g_depth_width-1 downto 0);
 
    rd_en_i   : in  std_logic;
    rd_data_o : out std_logic_vector(g_width-1 downto 0);
    empty_o   : out std_logic
    );
end fifo_linked_list;


architecture rtl of fifo_linked_list is

  type t_fifo_data is array (0 to g_depth - 1) of std_logic_vector(g_width - 1 downto 0);
  signal fifo_data : t_fifo_data := (others => (others => '0'));

  signal full  : std_logic;
  signal empty : std_logic;
  signal afull : std_logic;

begin
  full_o    <= full;
  afull_o   <= afull;
  empty_o   <= empty;

  rd_data_o <= fifo_data(0);

  ctrl : process (clk_i) is
    variable wr_idx   : integer range 0 to g_depth := 0;
    variable del_idx  : integer := 0;
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        fifo_data   <= (others => (others => '0'));
        del_data_o  <= (others => '0');
        full        <= '0';
        afull       <= '0';
        empty       <= '1';
        wr_idx      := 0;
        op_err_o    <= '0';
        cnt_o       <= (others => '0');
      else
        del_idx := to_integer(unsigned(del_idx_i));

        if (del_en_i = '0' and (wr_en_i = '1' or rd_en_i = '1')) then

          -- no delete, write or/and read
          if (wr_en_i = '1' and rd_en_i = '0' and full = '0') then
            fifo_data(wr_idx) <= wr_data_i;
            wr_idx            := wr_idx + 1;
            del_data_o        <= (others => '0');
            op_err_o          <= '0';
          elsif (wr_en_i = '0' and rd_en_i = '1' and empty = '0') then
            for  i in 0 to g_depth - 2 loop
              if (i <= wr_idx - 2) then
                fifo_data(i) <= fifo_data(i + 1);
              else
                fifo_data(i) <= fifo_data(i);
              end if;
            end loop;            
            wr_idx      := wr_idx - 1;
            del_data_o  <= (others => '0');
            op_err_o    <= '0';
          elsif (wr_en_i = '1' and rd_en_i = '1' and empty = '0') then
            fifo_data(wr_idx - 1) <= wr_data_i;

            for  i in 0 to g_depth - 2 loop
              if (i <= wr_idx - 2) then
                fifo_data(i) <= fifo_data(i + 1);
              else
                fifo_data(i) <= fifo_data(i);
              end if;
            end loop;
            wr_idx      := wr_idx;
            del_data_o  <= (others => '0');
            op_err_o    <= '0';
          else
            fifo_data   <= fifo_data;
            wr_idx      := wr_idx;
            del_data_o  <= (others => '0');
            op_err_o    <= '1';
          end if;
          -- delete or/and write or/and read
        elsif (del_en_i = '1' and del_idx < wr_idx and (afull = '1' or full = '1')) then
          if (wr_en_i = '0' and rd_en_i = '0' and empty = '0') then
            del_data_o  <= fifo_data(del_idx);
            
            for  i in 0 to g_depth - 2 loop
              if (i <= wr_idx - 3) then
                fifo_data(i) <= fifo_data(i + 1);
              else
                fifo_data(i) <= fifo_data(i);
              end if;
            end loop;            
            wr_idx := wr_idx - 1;
            op_err_o <= '0';
          elsif (wr_en_i = '1' and rd_en_i = '0') then
            del_data_o  <= fifo_data(del_idx);
            fifo_data(wr_idx - 1) <= wr_data_i;
            
            for  i in 0 to g_depth - 2 loop
              if (i <= wr_idx - 2) then
                fifo_data(i) <= fifo_data(i + 1);
              else
                fifo_data(i) <= fifo_data(i);
              end if;
            end loop;
            wr_idx := wr_idx;
            op_err_o <= '0';
          elsif (wr_en_i = '0' and rd_en_i = '1') then
            del_data_o  <= fifo_data(del_idx);

            for  i in 0 to g_depth - 3 loop
              if (i < del_idx - 1 or del_idx = 0) then
                fifo_data(i) <= fifo_data(i + 1);
              else
                fifo_data(i) <= fifo_data(i + 2);
              end if;
            end loop;
            wr_idx := wr_idx - 2;
            op_err_o <= '0';
          elsif (wr_en_i = '1' and rd_en_i = '1') then
            del_data_o  <= fifo_data(del_idx);
            fifo_data(wr_idx - 2) <= wr_data_i;
            
            for  i in 0 to g_depth - 3 loop
              if (i < del_idx - 1 or del_idx = 0) then
                fifo_data(i) <= fifo_data(i + 1);
              else
                fifo_data(i) <= fifo_data(i + 2);
              end if;
            end loop;
            wr_idx := wr_idx - 1;
            op_err_o <= '0';
          else
            fifo_data <= fifo_data;
            wr_idx := wr_idx;
            del_data_o  <= (others => '0');
            op_err_o    <= '1';
          end if;
        elsif (del_en_i = '0' and wr_en_i = '0' and rd_en_i = '0') then
          fifo_data   <= fifo_data;
          wr_idx      := wr_idx;
          del_data_o  <= (others => '0');
          op_err_o    <= '0';
        else
          fifo_data <= fifo_data;
          wr_idx      := wr_idx;
          del_data_o  <= (others => '0');
          op_err_o    <= '1';
        end if; -- del_en_i

        -- almost/full flags
        if (wr_idx >= g_depth) then
          full  <= '1';
          afull <= '0';
        elsif (wr_idx >= (g_depth - g_threshold)) then
          full  <= '0';
          afull <= '1';
        else
          full  <= '0';
          afull <= '0';
        end if;

        -- empty flag
        if (wr_idx <=  0) then
          empty <= '1';
        else
          empty <= '0';
        end if;
        
        cnt_o <= std_logic_vector(to_unsigned(wr_idx, g_depth_width));
      end if;
    end if;
    end process;
end rtl;
