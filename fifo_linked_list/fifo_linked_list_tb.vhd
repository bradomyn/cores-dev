library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity fifo_linked_list_tb is
end fifo_linked_list_tb;
 
architecture behave of fifo_linked_list_tb is
 
  constant c_depth : integer := 10;
  constant c_width : integer := 8;
  constant c_threshold : integer := 6;
  constant c_depth_width : integer := 4;
   
  signal r_reset   : std_logic := '0';
  signal r_clock   : std_logic := '1';
  signal r_wr_en   : std_logic := '0';
  signal r_wr_data : unsigned(c_width-1 downto 0);
  signal wr_data   : std_logic_vector(c_width-1 downto 0);
  signal w_full    : std_logic;
  signal w_afull   : std_logic;
  signal r_rd_en   : std_logic := '0';
  signal w_rd_data : std_logic_vector(c_width-1 downto 0);
  signal w_empty   : std_logic;
  signal d_idx     : std_logic_vector(c_depth_width - 1 downto 0) := (others => '0');
  signal d_en_i    : std_logic := '0';
  signal d_d_o     : std_logic_vector(c_width-1 downto 0);
  signal d_e_o     : std_logic;
  signal cnt       : std_logic_vector(c_depth_width - 1 downto 0);
     
  component fifo_linked_list is
  generic (
    g_width       : integer := 8;
    g_depth       : integer := 10;
    g_threshold   : integer := 6;
    g_depth_width : integer := 5
    );
  port (
    rst_n_i   : in std_logic;
    clk_i     : in std_logic;
 
    wr_en_i   : in  std_logic;
    wr_data_i : in  std_logic_vector(g_width-1 downto 0);
    full_o    : out std_logic;
    afull_o    : out std_logic;
    
    del_en_i  : in  std_logic;
    del_idx_i : in  std_logic_vector(g_depth_width-1 downto 0);
    del_data_o: out std_logic_vector(g_width-1 downto 0);

    op_err_o  : out std_logic;
    cnt_o     : out std_logic_vector(g_depth_width - 1 downto 0);

    rd_en_i   : in  std_logic;
    rd_data_o : out std_logic_vector(g_width-1 downto 0);
    empty_o   : out std_logic
    );
   end component fifo_linked_list;
   
begin
 
  fifo_del_inst : fifo_linked_list
    generic map (
      g_width       => c_width,
      g_depth       => c_depth,
      g_threshold   => c_threshold,
      g_depth_width => c_depth_width
      )
    port map (
      clk_i       => r_clock,
      rst_n_i     => r_reset,
      wr_en_i     => r_wr_en,
      wr_data_i   => wr_data,
      full_o      => w_full,
      afull_o     => w_afull,
      del_en_i    => d_en_i,
      del_idx_i   => d_idx,
      del_data_o  => d_d_o,
      op_err_o    => d_e_o,
      cnt_o       => cnt,
      rd_en_i     => r_rd_en,
      rd_data_o   => w_rd_data,
      empty_o     => w_empty
      );

  wr_data <= std_logic_vector(r_wr_data);
  r_clock <= not r_clock after 4 ns;

  data : process(r_clock) is
  begin
    if rising_edge(r_clock) then
      if r_reset = '0' then
          r_wr_data <= x"01";
      else
          if (r_wr_en = '1' and (w_full = '0' or r_rd_en = '1' or d_en_i = '1')) then
            r_wr_data <= r_wr_data + 1;
          end if;
      end if;
    end if;
  end process;
 
  p_test : process is
  begin
    wait until r_clock = '1';
    r_reset <= '0';
    wait until r_clock = '1'; 
    r_reset <= '1';
    wait until r_clock = '1';

    report("read when fifo empty");
    r_rd_en <= '1';
    wait for 32 ns;
    r_rd_en <= '0';

    report("write till the fifo is full and keep writing");
    wait for 16 ns;
    r_wr_en <= '1';
    wait for 32 ns;
    report("read & write");
    r_rd_en <= '1';
    wait for 8 ns;
    r_rd_en <= '0';
    wait for 160 ns;    
    r_wr_en <= '0';
    wait for 16 ns;
    report("write and read when fifo is full");
    r_rd_en <= '1';
    r_wr_en <= '1';
    wait for 8 ns;    
    report("keep reading till the fifo is empty");
    r_rd_en <= '1';
    r_wr_en <= '0';
    wait for 96 ns;
    report("write in the fifo");
    r_rd_en <= '0';
    r_wr_en <= '1';
    wait for 96 ns;
    report("delete elements till del error");
    r_wr_en <= '0';
    d_en_i <= '1';
    wait for 96 ns;
    report("write in the fifo till full");
    d_en_i <= '0';
    r_wr_en <= '1';
    wait for 112 ns;
    report("write and delete the last item when fifo is full");
    r_wr_en <= '1';
    d_en_i  <= '1';
    d_idx   <= std_logic_vector(to_unsigned(c_depth - 1, c_depth_width));
    wait for 96 ns;
    report("write and delete in 0 when fifo is full");
    r_wr_en <= '1';
    d_en_i  <= '1';
    d_idx   <= std_logic_vector(to_unsigned(0, c_depth_width));
    wait for 96 ns;
    r_wr_en <= '0';
    d_en_i  <= '0';
    report("read some cycles");
    r_rd_en <= '1';
    wait for 32 ns;
    report("write, read and delete all at the same time till error");
    d_idx   <= std_logic_vector(to_unsigned(2, c_depth_width));
    d_en_i  <= '1';
    r_rd_en <= '1';
    wait for 64 ns;
    report("same write");
    d_en_i  <= '0';
    r_rd_en <= '0';
    r_wr_en <= '1';
    wait for 64 ns;
    report("read and delate in 0");
    d_idx   <= (others => '0');
    r_wr_en <= '0';
    d_en_i  <= '1';
    r_rd_en <= '1';
    wait for 64 ns;    
    report("same write");
    d_en_i  <= '0';
    r_rd_en <= '0';
    r_wr_en <= '1';
    wait for 32 ns;
    report("write and delete error, trying to delete a non existing idx");
    r_wr_en <= '1';
    d_en_i  <= '1';
    d_idx   <= std_logic_vector(to_unsigned(c_depth - 1, c_depth_width));
    wait for 32 ns;
    report("write and delete up to error because the del idx is empty");
    d_idx   <= std_logic_vector(to_unsigned(4, c_depth_width));
    wait for 32 ns;
    assert false report "end of simulation" severity failure;
  end process;
   
end behave;
