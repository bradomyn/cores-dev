onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/g_width
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/g_depth
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/g_threshold
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/g_depth_width
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/clk_i
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/rst_n_i
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/wr_en_i
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/rd_en_i
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/op_err_o
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/del_en_i
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/full_o
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/afull_o
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/empty
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/wr_data_i
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/del_idx_i
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/del_data_o
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/cnt_o
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/rd_data_o
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/empty_o
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/fifo_data
add wave -noupdate /fifo_linked_list_tb/fifo_del_inst/full
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {102737 ps} 0}
configure wave -namecolwidth 148
configure wave -valuecolwidth 130
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1278312 ps}
