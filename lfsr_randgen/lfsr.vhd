----------------------------------------------------------------------------
---- Create Date:    13:06:08 07/28/2010 											----		
---- Design Name: lfsr																	----				
---- Project Name: lfsr_randgen													   ----	
---- Description: 																		----	
----  A random number generator based on linear feedback shift          ----
----  register(LFSR).A LFSR is a shift register whose input bit is a    ----
----  linear function of its previous state.The detailed documentation  ----	
----  is available in the file named manual.pdf.   							----	
----																							----	
----------------------------------------------------------------------------
----                                                                    ----
---- This file is a part of the lfsr_randgen project at                 ----
---- http://www.opencores.org/						                        ----
----                                                                    ----
---- Author(s):                                                         ----
----   Vipin Lal, lalnitt@gmail.com                                     ----
----                                                                    ----
----------------------------------------------------------------------------
----                                                                    ----
---- Copyright (C) 2010 Authors and OPENCORES.ORG                       ----
----                                                                    ----
---- This source file may be used and distributed without               ----
---- restriction provided that this copyright statement is not          ----
---- removed from the file and that any derivative work contains        ----
---- the original copyright notice and the associated disclaimer.       ----
----                                                                    ----
---- This source file is free software; you can redistribute it         ----
---- and/or modify it under the terms of the GNU Lesser General         ----
---- Public License as published by the Free Software Foundation;       ----
---- either version 2.1 of the License, or (at your option) any         ----
---- later version.                                                     ----
----                                                                    ----
---- This source is distributed in the hope that it will be             ----
---- useful, but WITHOUT ANY WARRANTY; without even the implied         ----
---- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR            ----
---- PURPOSE. See the GNU Lesser General Public License for more        ----
---- details.                                                           ----
----                                                                    ----
---- You should have received a copy of the GNU Lesser General          ----
---- Public License along with this source; if not, download it         ----
---- from http://www.opencores.org/lgpl.shtml                           ----
----                                                                    ----
----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.lfsr_pkg.ALL; 

entity lfsr is
   generic (g_width     : integer;
            g_with_mod  : boolean;
            g_auto_seed : boolean);
   port (
      clk_i       : in  std_logic;
      rst_n_i     : in  std_logic;
      set_seed_i  : in  std_logic;
      range_i     : in  std_logic_vector(g_width - 1 downto 0);
      seed_i      : in  std_logic_vector(g_width - 1 downto 0);
      rand_o      : out std_logic_vector(g_width - 1 downto 0));
end lfsr;

architecture rtl of lfsr is
   
   begin
   
   rnd_gen : process(clk_i)
      variable rand_temp   : std_logic_vector (g_width - 1 downto 0):=(0 => '1',others => '0');
      variable temp        : std_logic;
   begin
         
      if(rising_edge(clk_i)) then
         if (rst_n_i = '0') then
            rand_temp := (0 => '1',others => '0');
            temp      := '0';
            rand_o    <= (others => '0');
         else
            if(set_seed_i = '1' and g_auto_seed = false) then
               rand_temp := seed_i;
            end if;
            temp := xor_gates(rand_temp);
            rand_temp(g_width - 1 downto 1) := rand_temp(g_width - 2 downto 0);
            rand_temp(0) := temp;

            if (g_with_mod = true) then
              rand_o  <= min_module(g_width, range_i, rand_temp);
            else
              rand_o  <= rand_temp;
            end if;
         end if;
      end if;
   end process;
end rtl;
