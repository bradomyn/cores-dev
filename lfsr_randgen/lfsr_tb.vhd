----------------------------------------------------------------------------
---- Create Date:    20:14:07 07/28/2010 											----		
---- Design Name: lfsr_tb																----				
---- Project Name: lfsr_randgen													   ----	
---- Description: 																		----	
----  A testbench code for the lfsr.vhd code                            ----
----																							----	
----------------------------------------------------------------------------
----                                                                    ----
---- This file is a part of the lfsr_randgen project at                 ----
---- http://www.opencores.org/						                        ----
----                                                                    ----
---- Author(s):                                                         ----
----   Vipin Lal, lalnitt@gmail.com                                     ----
----                                                                    ----
----------------------------------------------------------------------------
----                                                                    ----
---- Copyright (C) 2010 Authors and OPENCORES.ORG                       ----
----                                                                    ----
---- This source file may be used and distributed without               ----
---- restriction provided that this copyright statement is not          ----
---- removed from the file and that any derivative work contains        ----
---- the original copyright notice and the associated disclaimer.       ----
----                                                                    ----
---- This source file is free software; you can redistribute it         ----
---- and/or modify it under the terms of the GNU Lesser General         ----
---- Public License as published by the Free Software Foundation;       ----
---- either version 2.1 of the License, or (at your option) any         ----
---- later version.                                                     ----
----                                                                    ----
---- This source is distributed in the hope that it will be             ----
---- useful, but WITHOUT ANY WARRANTY; without even the implied         ----
---- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR            ----
---- PURPOSE. See the GNU Lesser General Public License for more        ----
---- details.                                                           ----
----                                                                    ----
---- You should have received a copy of the GNU Lesser General          ----
---- Public License along with this source; if not, download it         ----
---- from http://www.opencores.org/lgpl.shtml                           ----
----                                                                    ----
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.math_real.all;

library work;
use work.lfsr_pkg.all;
 
entity lfsr_tb is
end lfsr_tb;
 
architecture behavior of lfsr_tb is     

   signal clk,set_seed  : std_logic := '0';
   signal seed          : std_logic_vector(c_width - 1 downto 0) := (0 => '1',others => '0');
   signal rand_out      : std_logic_vector(c_width - 1 downto 0);
   signal rst_n         : std_logic;
   signal range_value   : std_logic_vector(c_width - 1 downto 0) := (others => '0');
   -- clock period definitions
   constant clk_period  : time    := 8 ns;
   constant c_with_mod  : boolean := false;
   constant c_auto_seed : boolean := true;
 
begin

	-- entity instantiation for the lfsr component.
   uut: lfsr generic map (
         g_width      => c_width,
         g_with_mod   => c_with_mod,
         g_auto_seed  => c_auto_seed)
	   port map (
          clk_i      => clk,
          rst_n_i    => rst_n,
			    set_seed_i => set_seed,
          range_i    => range_value,
          seed_i     => seed,
          rand_o     => rand_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   -- Applying stimulation inputs.
   stim_proc: process
   begin
      rst_n    <= '0';
      wait for 8 ns;
      rst_n    <= '1';
      wait for 8 ns;
      set_seed <= '1';	
      wait for 8 ns;
      set_seed <= '0';
      wait for 1600 ns;
      assert false report "end of simulation" severity failure;
   end process;
end;

