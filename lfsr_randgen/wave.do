onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /lfsr_tb/uut/g_width
add wave -noupdate /lfsr_tb/uut/clk_i
add wave -noupdate /lfsr_tb/uut/rst_n_i
add wave -noupdate /lfsr_tb/uut/set_seed_i
add wave -noupdate /lfsr_tb/uut/range_i
add wave -noupdate /lfsr_tb/uut/seed_i
add wave -noupdate /lfsr_tb/uut/rand_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {40039 ps} 0}
configure wave -namecolwidth 134
configure wave -valuecolwidth 126
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1368640 ps} {1637440 ps}
