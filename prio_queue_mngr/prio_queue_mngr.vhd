library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.prio_queue_mngr_pkg.all;


entity prio_queue_mngr is
  generic (
    g_queue_num         : integer;
    g_queue_data_width  : integer;
    g_queue_len         : integer;
    g_queue_afull_thold : integer
    );
  port (
    clk_i           : in  std_logic;
    rst_n_i         : in  std_logic;
    queue_wr_en_i   : in  std_logic_vector(g_queue_num - 1 downto 0);
    queue_wr_dat_i  : in  std_logic_vector(g_queue_data_width - 1 downto 0);
    queue_rd_en_i   : in  std_logic_vector(g_queue_num - 1 downto 0);
    queue_rd_dat_o  : out std_logic_vector(g_queue_data_width - 1 downto 0);
    queue_empty_o   : out std_logic_vector(g_queue_num - 1 downto 0);
    queue_valid_o   : out std_logic;
    queue_drop_o    : out std_logic;
    queue_drop_dat_o: out std_logic_vector(g_queue_data_width - 1 downto 0)
    );
end prio_queue_mngr;

architecture rtl of prio_queue_mngr is


  constant c_queue_len_width  : integer :=  f_log2_size(g_queue_len);
  constant c_auto_seed        : boolean := true; 

  type t_queue_len  is array (g_queue_num - 1 downto 0) of std_logic_vector(c_queue_len_width - 1 downto 0);
  type t_queue_data is array (g_queue_num - 1 downto 0) of std_logic_vector(g_queue_data_width - 1 downto 0);

  signal full   : std_logic_vector(g_queue_num - 1 downto 0);
  signal afull  : std_logic_vector(g_queue_num - 1 downto 0);
  signal wr_cnt : std_logic_vector(c_queue_len_width - 1 downto 0) := (others => '0');
  signal rr_cnt : integer range 0 to g_queue_num - 1;
  signal rd_cnt : integer range 0 to g_queue_num - 1;
  signal seed   : std_logic_vector(c_queue_len_width - 1 downto 0) := (others => '0');
  signal rnd_del_idx    : std_logic_vector(c_queue_len_width - 1 downto 0);
  signal queue_op_err   : std_logic_vector(g_queue_num - 1 downto 0);  
  signal queue_del_idx  : std_logic_vector(c_queue_len_width - 1 downto 0);
  signal queue_del_en   : std_logic_vector(g_queue_num - 1 downto 0);
  signal queu_del_idx   : t_queue_len;
  signal queue_wr_cnt   : t_queue_len;
  signal queue_rd_dat   : t_queue_data;
  signal queue_del_dat  : t_queue_data;

  begin

  queue_rd_dat_o  <= queue_rd_dat(to_integer(unsigned(queue_rd_en_i)));
  queue_valid_o   <= queue_rd_en_i(to_integer(unsigned(queue_rd_en_i))) and
                    (not queue_op_err(to_integer(unsigned(queue_rd_en_i))));

  PRIO_QUEUES : for i in 0 to g_queue_num - 1 generate 
    QUEUE : fifo_linked_list
      generic map (
        g_width       => g_queue_data_width,
        g_depth       => g_queue_len,
        g_threshold   => g_queue_afull_thold,
        g_depth_width => c_queue_len_width)
      port map (
        clk_i       => clk_i,
        rst_n_i     => rst_n_i,
        wr_en_i     => queue_wr_en_i(i),
        wr_data_i   => queue_wr_dat_i,
        full_o      => full(i),
        afull_o     => afull(i),
        empty_o     => queue_empty_o(i),
        del_en_i    => queue_del_en(i),
        del_idx_i   => queue_del_idx,
        del_data_o  => queue_del_dat(i),
        op_err_o    => queue_op_err(i),
        cnt_o       => queue_wr_cnt(i),
        rd_en_i     => queue_rd_en_i(i),
        rd_data_o   => queue_rd_dat(i));
  end generate;
  
  -- linear random generator
  DROP_IDX_GEN  : lfsr 
      generic map (
        g_width     => c_queue_len_width,
        g_with_mod  => c_with_mod,
        g_auto_seed => c_auto_seed)
      port map (
        clk_i      => clk_i,
        rst_n_i    => rst_n_i,
        set_seed_i => '0',
        range_i    => wr_cnt,
        seed_i     => seed,
        rand_o     => rnd_del_idx);

  RR_QUEUE_DROP  : process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (rst_n_i = '0') then
        rr_cnt            <= 1;
        rd_cnt            <= 0;
        queue_del_en      <= (others => '0');
        queue_del_idx     <= (others => '0');
        queue_drop_dat_o  <= (others => '0');
        queue_drop_o      <= '0';
      else

        if (rr_cnt /= g_queue_num - 1) then
          rr_cnt <= rr_cnt + 1;
        else
          rr_cnt <= 0;
        end if;
        rd_cnt <= rr_cnt;
        
        -- check if this queue needs to drop packets
        if (afull(rr_cnt) = '1') then
          queue_del_en(rr_cnt)  <= '1';
          queue_del_idx         <= min_module(c_queue_len_width, queue_wr_cnt(rr_cnt), rnd_del_idx);
        else
          queue_del_en(rr_cnt)  <= '0';
          queue_del_idx         <= (others => '0');
        end if;
        queue_del_en(rd_cnt)  <= '0';

        -- if the previous checked queue has to drop, propagate the value
        if(queue_del_en(rd_cnt) = '1' and queue_op_err(rd_cnt) = '0') then
          queue_drop_dat_o  <= queue_del_dat(rd_cnt);
          queue_drop_o      <= '1';
        else
          queue_drop_dat_o  <= (others => '0');
          queue_drop_o      <= '0';
        end if;
      end if;
    end if;
  end process;
end rtl;
