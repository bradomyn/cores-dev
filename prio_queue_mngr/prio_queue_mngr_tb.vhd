library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
library work;
use work.prio_queue_mngr_pkg.all;

entity prio_queue_mngr_tb is
end prio_queue_mngr_tb;
 
architecture sim of prio_queue_mngr_tb is

  constant c_queue_num          : integer := 8;
  constant c_queue_data_width   : integer := 16;
  constant c_queue_len          : integer := 64;
  constant c_queue_afull_thold  : integer := 32;  

  signal rst_n      : std_logic := '0';
  signal clk        : std_logic := '1';

  -- fifo
  signal wr_en      : std_logic_vector(c_queue_num - 1 downto 0);
  signal empty      : std_logic_vector(c_queue_num - 1 downto 0);
  signal u_wr_data  : unsigned(c_queue_data_width - 1 downto 0);
  signal wr_data    : std_logic_vector(c_queue_data_width - 1 downto 0);
  signal rd_en      : std_logic_vector(c_queue_num - 1 downto 0);
  signal rd_data    : std_logic_vector(c_queue_data_width - 1 downto 0);
  signal rd_valid   : std_logic;
  signal drop       : std_logic;  
  signal drop_data  : std_logic_vector(c_queue_data_width - 1 downto 0);

begin

  PRIO_QUEUE : prio_queue_mngr
  generic map (
    g_queue_num         => c_queue_num,
    g_queue_data_width  => c_queue_data_width,
    g_queue_len         => c_queue_len,
    g_queue_afull_thold => c_queue_afull_thold)
  port map (
    clk_i             =>  clk,
    rst_n_i           =>  rst_n,
    queue_wr_en_i     =>  wr_en,
    queue_wr_dat_i    =>  wr_data,
    queue_rd_en_i     =>  rd_en,
    queue_rd_dat_o    =>  rd_data,
    queue_empty_o     =>  empty,
    queue_valid_o     =>  rd_valid,
    queue_drop_o      =>  drop,
    queue_drop_dat_o  =>  drop_data);

  clk <= not clk after 4 ns;

  data : process(clk) is
  begin
    if rising_edge(clk) then
      if rst_n = '0' then
        u_wr_data <= x"0001";
      else
        u_wr_data <= u_wr_data + 1;
      end if;
    end if;
  end process;
 
  wr_data <= std_logic_vector(u_wr_data);

  p_test : process is
  begin
    wait until clk = '1';
    rst_n <= '0';
    wait until clk = '1'; 
    rst_n <= '1';
    wait until clk = '1';

    report("write in");

    wr_en   <= "00000010";
    rd_en   <= "00000000";
    wait for 8 ns;
    wr_en   <= "00000001";
    rd_en   <= "00000000";
    wait for 8 ns;
    wr_en   <= "00000100";    
    rd_en   <= "00000000";
    wait for 8 ns;
    wr_en   <= "00000100";
    rd_en   <= "00000000";
    wait for 400 ns;
    wr_en   <= "00000000";
    rd_en   <= "00000000";
    wait for 64 ns;
    wr_en   <= "00000100";
    rd_en   <= "00000000";
    wait for 32 ns;
    wr_en   <= "00000000";
    rd_en   <= "00000100";
    wait for 800 ns;
    assert false report "end of simulation" severity failure;
  end process;
   
end sim;
