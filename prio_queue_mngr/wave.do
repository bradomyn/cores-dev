onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/c_queue_afull_thold
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/c_queue_data_width
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/c_queue_len
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/c_queue_num
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/clk
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/drop
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/drop_data
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/rd_data
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/rd_en
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/rd_valid
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/rst_n
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/u_wr_data
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/wr_data
add wave -noupdate -expand -group PRIO_QUEUE_MNGR_TB /prio_queue_mngr_tb/wr_en
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/g_queue_num
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/g_queue_data_width
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/g_queue_len
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/g_queue_afull_thold
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/clk_i
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/rst_n_i
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_wr_en_i
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_wr_dat_i
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_rd_en_i
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_rd_dat_o
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_valid_o
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_drop_o
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_drop_dat_o
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/full
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/afull
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/wr_cnt
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/rr_cnt
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/rd_cnt
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/seed
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/rnd_del_idx
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_op_err
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_del_idx
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_del_en
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queu_del_idx
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_wr_cnt
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_rd_dat
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/queue_del_dat
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/c_queue_len_width
add wave -noupdate -expand -group RND /prio_queue_mngr_tb/PRIO_QUEUE/c_auto_seed
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/wr_en_i
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/wr_data_i
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/full_o
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/afull_o
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/del_en_i
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/del_idx_i
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/del_data_o
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/op_err_o
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/cnt_o
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/rd_en_i
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/rd_data_o
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/empty_o
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/fifo_data
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/full
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/empty
add wave -noupdate -group QUEUE_0 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(0)/QUEUE/afull
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/wr_en_i
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/wr_data_i
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/full_o
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/afull_o
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/del_en_i
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/del_idx_i
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/del_data_o
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/op_err_o
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/cnt_o
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/rd_en_i
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/rd_data_o
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/empty_o
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/fifo_data
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/full
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/empty
add wave -noupdate -group QUEUE_1 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(1)/QUEUE/afull
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/wr_en_i
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/wr_data_i
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/full_o
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/afull_o
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/del_en_i
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/del_idx_i
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/del_data_o
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/op_err_o
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/cnt_o
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/rd_en_i
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/rd_data_o
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/empty_o
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/fifo_data
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/full
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/empty
add wave -noupdate -group QUEUE_2 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(2)/QUEUE/afull
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/wr_en_i
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/wr_data_i
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/full_o
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/afull_o
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/del_en_i
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/del_idx_i
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/del_data_o
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/op_err_o
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/cnt_o
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/rd_en_i
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/rd_data_o
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/empty_o
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/fifo_data
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/full
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/empty
add wave -noupdate -group QUEUE_3 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(3)/QUEUE/afull
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/wr_en_i
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/wr_data_i
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/full_o
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/afull_o
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/del_en_i
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/del_idx_i
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/del_data_o
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/op_err_o
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/cnt_o
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/rd_en_i
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/rd_data_o
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/empty_o
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/fifo_data
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/full
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/empty
add wave -noupdate -group QUEUE_4 /prio_queue_mngr_tb/PRIO_QUEUE/PRIO_QUEUES(4)/QUEUE/afull
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {19005 ps} 0}
configure wave -namecolwidth 224
configure wave -valuecolwidth 90
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1077628 ps} {1336 ns}
